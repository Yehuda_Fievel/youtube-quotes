# Run Server

From the main directory run the following command to build the docker image:
  -  `docker build -t youtube-quotes-server ./server`.

To run the server run the following command:
  -  `docker run -p 3000:3000 -d youtube-quotes-server`
