import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SongsApiService } from '../../services/songs-api.service';

@Component({
  selector: 'songs-list',
  templateUrl: './songs-list.component.html',
  styleUrls: ['./songs-list.component.scss']
})
export class SongsListComponent implements OnInit {
	@Output() onSongChange = new EventEmitter<any>();

	public songs: Array<any>;
	public recommendations: Array<any>;
	public selectedSong: any;
	public query: string;
	public search: string;

    constructor(private songsApiService: SongsApiService) {}
    
    ngOnInit(): void {
		this.songsApiService.getAllSongs().then(results => {
			this.songs = results.songs;
			this.selectSong(this.songs[0]);
		});
	}

	public searchSong(query) {
		this.songsApiService.getAllSongs(query).then(results => {
			this.songs = results.songs;
			this.recommendations = results.recomendations;
			console.log(this.recommendations);
		});
	}
	
	public selectSong(song) {
		this.selectedSong = song;
		
		this.onSongChange.emit(song);
	}
}
