import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class SongsApiService {

	public async getAllSongs(query?: string): Promise<any> {
		let url = query ?  `http://localhost:3000/videos?q=${query}` : 'http://localhost:3000/videos'
		let response = await fetch(url);
		return response.json();
	}
}
