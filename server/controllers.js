const allSongs = require('./songs.json')


function get(req, res, next) {
    try {
        let songs = allSongs
        let recommendations = []
        if (req.query.q) {
            songs = allSongs.filter(song => song.title.toLowerCase().indexOf(req.query.q.toLowerCase()) !== -1)
            if (req.query.q.length > 2) {
                recommendations = allSongs.filter(song => {
                    return song.quotes.filter(quote => {
                        return quote.text.toLowerCase().indexOf(req.query.q.toLowerCase()) !== -1
                    }).length
                })
            }
        }
        res.status(200).json({ songs, recommendations })
    } catch (error) {
        console.log(error)
        res.sendStatus(500)
    }
}

module.exports = {
    get,
}